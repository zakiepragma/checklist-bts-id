import React, { useState } from 'react'
import {Card, Button, Container, Form, Alert} from 'react-bootstrap';
import axios from 'axios';

const Register = () => {
    const[email, setEmail] = useState("");
    const[password, setPassword] = useState("");
    const[username, setUsername] = useState("");
    const[emailError, setEmailError] = useState('');
    const[passwordError, setPasswordError] = useState('');
    const[usernameError, setUsernameError] = useState('');
    const[loading, setLoading] = useState(false);
    const[variantnya, setVariantnya] = useState('');
    const[namanya, setNamanya] = useState('');
    const[contentnya, setContentnya] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        setVariantnya('');
        setNamanya("");
        setContentnya("");
        if(email){
            setEmailError("");
            if(password){
                setPasswordError("")
                if(username){
                    setUsernameError("")
                    setLoading(true);
                    //register
                    // axios({
                    //     method:'POST',
                    //     url:'http://94.74.86.174:8080/api/register',
                    //     timeout:120000,
                        
                    //     data:{email:email, password:password, username:username}
                    // }).then((response)=>{
                    //     setLoading(false);
                    //     setVariantnya('success');
                    //     setNamanya("Succes Register");
                    //     setContentnya("Selamat, anda berhasil register...");
                    // }).catch((error)=>{
                    //     setLoading(false);
                    //     setVariantnya('danger');
                    //     setNamanya("Gagal Register");
                    //     setContentnya("Maaf, anda gagal register...");
                    // })

                    try {
                        const resp = await axios.post('http://94.74.86.174:8080/api/register',
                            {email,password,username},
                            {
                                headers: {
                                'Content-Type': 'application/json'
                                }
                            }
                        )
                        setLoading(false);
                        setVariantnya('success');
                        setNamanya("Succes Register");
                        setContentnya("Selamat, anda berhasil register...");
                    } catch (error) {
                        setLoading(false);
                        setVariantnya('danger');
                        setNamanya("Gagal Register");
                        setContentnya("Maaf, anda gagal register...");
                    }

                }else{
                    setUsernameError("Username wajib diisi")
                }
            }else{
                setPasswordError("Password wajib diisi")
            }
        }else{
            setEmailError("E-Mail wajib diisi");
        }
    }

    function Informasi({variant, name, content}) {
        if(variantnya != ""){
            return <Alert variant={variant}>
            <Alert.Heading>{name}</Alert.Heading>
            <p>
              {content}
            </p>
          </Alert>
        }
    }

    const ButtonSubmit = ({loading}) => {

        if(loading==true){
            return <Button variant="primary" disabled>Loading...</Button>
        }
        return (
            <Button variant="primary" type="submit" className='btn'>Register</Button>
        )
    }

  return (
    <Container className='mt-3'>
        <Card>
            <Card.Header>BTS.ID App</Card.Header>
            <Card.Body>
                <Card.Title>Register</Card.Title>
                <Informasi variant={variantnya} name={namanya} content={contentnya}/>
                <Form onSubmit={(event)=>handleSubmit(event)}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" onChange={(e)=>setEmail(e.target.value)}/>
                    <Form.Text className="text-danger">
                    {emailError}
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={(e)=>setPassword(e.target.value)}/>
                    <Form.Text className="text-danger">
                    {passwordError}
                    </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="Username" onChange={(e)=>setUsername(e.target.value)}/>
                    <Form.Text className="text-danger">
                    {usernameError}
                    </Form.Text>
                </Form.Group>
                <ButtonSubmit loading={loading}/>
                </Form>
            </Card.Body>
        </Card>
    </Container>
  )
}

export default Register